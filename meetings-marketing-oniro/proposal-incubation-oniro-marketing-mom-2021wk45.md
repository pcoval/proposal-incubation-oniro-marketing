# Oniro Incubation Marketing Meeting 

## Agenda 

Meeting main goal: introduction and preparation of the coming f2f meeting next 
week.

* First meeting. Intro and Expectations. Agustin 5 min 
* Preparation of the f2f Oniro Dev Summit
   * Expectations for this event/meetings. 
   * Evaluation and impact of the Oniro launch and EclipseCon 2021
   * Marketing OKRs for 2022 as part of the Oniro Program Plan.
   * 2022 Events list. 
* OSXP. 2021 Impact.
* SFSCon 2021 and Oniro Dev Summit preparations.

Note: this is the first meeting off the series. The agenda has not been sent 
in advance. The idea is to define the agenda as the first point of the meeting. 
The above is just a proposal for efficiency.

## MoM

Participants: Shanda G., Clark R., Davide R., Andrea B., Gianluca V., Carlo P., 
Agustin B.B. and Patrick O.

### First meeting. Intro and Expectations. Agustin 5 min

Proposal

* Ground rules during the incubation period proposed by Agustin
   * Meeting goals
      * Operational meeting: preparations for the formal 
meeting.
   * Formal meeting: Marketing Committee kick off.
	* Rotation of chair and minutes taker.
   * Formal meeting how-to: same formalism as the Marketing and Brand Committee 
once formed.
      * Check the Oniro Charter: 
https://www.eclipse.org/org/workinggroups/oniro-charter.php 
		* Check the WG Operations document: 
https://www.eclipse.org/org/workinggroups/operations.php#h.exw3gowdu1xz 
   * Operational meeting: agenda sent to the oniro-wg two days in advance.
   * Agendas, MoM and material published at the corresponding Oniro WG gitlab 
repo and communicated through the oniro-wg mailing list.

Discussion

Davide provides an overview of the topics that should be discussed on the 
operational series of these meetings. The activities of this group should be 
linked to the business KPIs.   

Clark explains the approach the EF follows

### Preparation of the f2f Oniro Dev Summit

#### Expectations for this event/meetings.

Proposal

What would we like to achieve during the Oniro Dev Summit?

Agustin:
* Define the goals to propose to the SC for 2022 as part of the Program Plan 
preparations.
* Define OKRs and other key metrics to evaluate our progress. 
* Key actions for 2022
   * 2022 events list
   * Campaigns
   * Web page

Discussion

Davide: Huawei has been the main driver so far. The time to bring other parties 
to drive these marketing activities has arrive. We need volunteers to drive the 
key actions: roles and responsibilites. Agree on what we will do up to the kick 
off.

Andrea Basso asks how is the pipeline management and budget works. Agustin 
explains it. EF will support the SC and marketing group to work in the right 
way.


#### Evaluation and impact of the Oniro launch and EclipseCon 2021

Proposal

* EF will bring some data to the event.
* Request to members to share the impact of their collaboration/participation 
had on their end.

Discussion

Agustin: send a reminder to this group about bringing their output from the 
effort in promoting Oniro. 


#### 2022 Events list.

Proposal

* Each organization brings a list of the events they are interested or already 
participate on. We will us such list as starting point.
* Organise the list based on criteria like who drives the 
participation/investment, expected impact, costs, etc. so we can prioritise.
* The goal is to agree on such list during 2021. Is it possible?

Discussion

Davide: events list from Huawei will be ready in December. Huawei 2021 list 
could be a starting point.

Gianluca describes his approach to the Marketing effort. SECO welcome input 
from others and a discussion among the interested organization in Oniro about 
how to invest in a smart way in this marketing area.


Moved to the parking lot due to lack of time

### OSXP. 2021 Impact

Proposal

Discussion

### SFSCon 2021 and Oniro Dev Summit preparations.

Proposal

* List of attendees
* Social media campaign
   * Some messages already published from @oniro_project

Discussion
