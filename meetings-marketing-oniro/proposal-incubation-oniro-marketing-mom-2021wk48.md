# Oniro WG Incubation stage marketing wk48

Chair: Agustin
Scriba: Agustin
Schedule: 2021-12-01 #link 
https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage- 
oniro/-/wikis/Meetings

## Participants

(add/erase participants)

Chiara (Huawei), Shanda (EF), Yves (EF), Gianluca V.(SECO), Aurore P. 
(Huawei), Ebba (Linaro), Dony (Huawei), Agustin B.B.(EF) Alessandra (SECO), 
Patrick O. (NOI) and Carlo P.(Array), Andrea G. (Linaro)

## Agenda

* Value Proposition - Gianluca 15 min
* events List - Chiara 5 min
* Walk-through Gitlab - Agustin 5 min
* AOB - 5 min

## MoM

### Value Proposition - Gianluca/Yves 15 min

* A group of people is getting together to help Gianluca. A meeting has 
been held already. What are the values? Answering this question is part of the 
work.
* Chiara is working on the main key messages based on the above discussion.
* This group has regular meetings: Yves, Chiara, Gianluca, Olivier, Aurore and 
Alessandra. 
   * Contact them to participate in the group. Gael wants to participate. 
Agustin will send content.
* We need personas to test the value proposition with SECO customers, for 
instance. Persona: 
   * exec. Main target is new business
   * Operational and R&D: cost savings
* The output will be socialised in the supporters meetings.

### Events List - Chiara 5 min

* Nothing to share today. 
* NOI sent the list to Chiara
* CfPs deadlines has to be part of the input information.
   * Dates, CfP information, dates and if your organization is attending 
anyway. With a booth?


### Walk through Gitlab - Agustin 5 min

Presentation

* Mail sent to the oniro-wg mailing list with the most relevant links: 
#link https://www.eclipse.org/lists/oniro-wg/msg00028.html 
* Next steps:
   * Basic guidelines on how to use the different elements on gitlab.
   * Upload content from EclipseCon 2021

Discussion

No time for this topic.

### AOB - 5 min

* Guidelines - Agustin
   * General guidelines from EF: 
#link https://www.eclipse.org/org/documents/social_media_guidelines.php
   * #task Agustin will add the above and others about how to interact with 
existing resources and channels in the wiki: #link 
https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-
oniro/-/wikis/home
* Request for a new meeting for marketing operational topics. Rationale sent by 
mail - Chiara
   * Chiara: Working regular meeting. Also onboarding. Ebba supports Chiara 
proposal.
   * Agustin does not supports the idea of creating more regular meetings. We 
already have three (105 min weekly).
   * #agreement We go for a task force for marketing priorities. We take this 
proposal from there. Chiara will drive it.
   * Gael. Task force meetings should be published on the Oniro calendar.
* Join the mailing list oniro-wg: #link 
https://accounts.eclipse.org/mailing-list/oniro-wg 
   * Archive: #link https://www.eclipse.org/lists/oniro-wg/maillist.html
   * RSS: #link http://dev.eclipse.org/mhonarc/lists/oniro-wg/maillist.rss 
